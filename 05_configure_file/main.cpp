#include <iostream>

#include "version.hpp"

int main()
{
    std::cout << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
    exit(EXIT_SUCCESS);
}
