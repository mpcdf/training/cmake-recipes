#include <iostream>

/**
 * Add up two integer numbers and return the result.
 *
 * @param lhs first summand
 * @param rhs second summand
 * @return result of the addition
 */
int add(int lhs, int rhs) { return lhs + rhs; }

int main()
{
    std::cout << add(3, 4) << std::endl;
    return EXIT_SUCCESS;
}
