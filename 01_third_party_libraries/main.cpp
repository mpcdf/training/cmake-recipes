#include <omp.h>

#include <iostream>

int main(int /*argc*/, char** /*argv*/)
{
    std::cout << omp_get_max_threads() << std::endl;
    exit(EXIT_SUCCESS);
}
