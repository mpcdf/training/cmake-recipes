# How to get the current git hash at configure time?

With [find_package](https://cmake.org/cmake/help/latest/command/find_package.html)
we can locate the git executable followed by
[execute_process](https://cmake.org/cmake/help/latest/command/execute_process.html)
to get the current commit hash. The result is stored in a CMake variable. The git hash is
updated whenever CMake is run, i.e. at configure time.
This can be combined with [How to pass information from CMake to your code?](../05_configure_file)
to expose the git hash to the source code.