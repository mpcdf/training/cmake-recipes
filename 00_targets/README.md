# Think in targets!

This recipe illustrates how to build a basic application. 
It consists of a backend library and an application frontend.

## What is a target?
Targets provide modularization and encapsulation. A target
encapsulates everything needed for a certain build job. It also
is some kind of container that allows efficient use as a building 
block.

## Define the target type
[add_library](https://cmake.org/cmake/help/latest/command/add_library.html)  
[add_executable](https://cmake.org/cmake/help/latest/command/add_executable.html)

## Set target properties
| Aim                                    | Command                                                                                                   |
|----------------------------------------|-----------------------------------------------------------------------------------------------------------|
| add definitions                        | [target_compile_definitions](https://cmake.org/cmake/help/latest/command/target_compile_definitions.html) | 
| add compile flags                      | [target_compile_options](https://cmake.org/cmake/help/latest/command/target_compile_options.html)         |
| add include directories                | [target_include_directories](https://cmake.org/cmake/help/latest/command/target_include_directories.html) |
| add a dependency, link against library | [target_link_libraries](https://cmake.org/cmake/help/latest/command/target_link_libraries.html)           |
| add linker options                     | [target_link_options](https://cmake.org/cmake/help/latest/command/target_link_options.html)               |
| add sources                            | [target_sources](https://cmake.org/cmake/help/latest/command/target_sources.html)                         |

## Usage requirements
https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html#transitive-usage-requirements

| keyword       | result                                                       |
|---------------|--------------------------------------------------------------|
| **PRIVATE**   | required to build the target, but not required when using it |
| **INTERFACE** | not required to build the target, but required when using it |
| **PUBLIC**    | required for for building the target and for using it        |

## How to link against a "supported" third-party library?
Find the library with [find_package](https://cmake.org/cmake/help/latest/command/find_package.html) and link against 
the imported target with [target_link_libraries](https://cmake.org/cmake/help/latest/command/target_link_libraries.html).