# How to get the current git hash at build time?

We will use the ability of CMake to run as an interpreter 
[interpreter](https://cmake.org/cmake/help/latest/manual/cmake.1.html#run-a-script)
to retrieve the git hash at runtime.  
The idea works as follows:
 * Use [How to get the current git hash at configure time?](06_git_configure) to get the git hash.
 * Use [How to pass information from CMake to your code?](05_configure_file) to write the hash
   into a source file.
 * Move the logic into a separate CMake [file](cmake/update_git_hash.cmake).
 * Use [add_custom_command](https://cmake.org/cmake/help/latest/command/add_custom_command.html)
   to invoke the [CMake interpreter](https://cmake.org/cmake/help/latest/manual/cmake.1.html#run-a-script).
 * Move everything into its own target to allow parallel execution: 
   [add_custom_target](https://cmake.org/cmake/help/latest/command/add_custom_target.html).
 * Add a [dependency](https://cmake.org/cmake/help/latest/command/add_dependencies.html)
   so it gets executed whenever the project is rebuild.

Note, we configure a cpp file here to avoid recompilation of huge portions of the code!