# How to install a project with CMake?

This recipe consists of two folders. `external` resembles some third-party library. `internal` is your project
consisting of a library and a frontend executable.

## How to define your targets to facilitate installation?
Since CMake 3.23 supports [FILE_SET](https://cmake.org/cmake/help/latest/command/target_sources.html#command:target_sources).
If you define your header files within the `HEADERS` file set. They will be automatically installed into the include
directory. If you use a lower CMake version you need to take care to install the headers yourself.

## Install the files
All targets that need to be installed need an [install](https://cmake.org/cmake/help/latest/command/install.html#targets)
statement. For installing files manually (for example, header files if you do not use file sets) you can use a different 
version of the [install](https://cmake.org/cmake/help/latest/command/install.html#files) command. With these install commands
you can already install your library and application. However, if someone wants to include your library they need
to manually link or write a find script. CMake has the concept of config files to solve this problem.

## How to create config files?
CMake can automatically generate files that import your targets. For this you can use another version of the 
[install](https://cmake.org/cmake/help/latest/command/install.html#export) command. If you have external dependencies,
you have to make sure they are available when someone links against your library. This is done using
[find_dependency](https://cmake.org/cmake/help/latest/module/CMakeFindDependencyMacro.html). You can see how all of this
looks like in the external subfolder.

## Installing into non-standard locations and linking against dependencies in non-standard locations.
A design decision of CMake is to clear all `rpath`s when you install executables or shared libraries. The idea behind this
is everything gets installed into standard locations and can be found at runtime. It also makes sure you do not link against
libraries in your build folder. This is bad practice as one should be able to delete the build folder after installation.

However, in some situations this behaviour is unwanted. For example, if you link against libraries installed in
non-standard locations. In this case you will not find the libraries at runtime. CMake can keep all `rpath`s pointing
into locations that are not within your build folder. This is achieved by 
[CMAKE_INSTALL_RPATH_USE_LINK_PATH](https://cmake.org/cmake/help/latest/variable/CMAKE_INSTALL_RPATH_USE_LINK_PATH.html).

Unfortunately your installation might still fail if you have a local library in your build folder and install into
a non-standard location. Then this library will not be found at runtime. You can solve this by manually specifying your
installation directory as a rpath. [CMAKE_INSTALL_RPATH](https://cmake.org/cmake/help/latest/variable/CMAKE_INSTALL_RPATH.html)