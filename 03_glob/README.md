# How to automatically collect all source files?

CMake has the functionality to search the file system for certain files using the
[file](https://cmake.org/cmake/help/latest/command/file.html#glob) command.
The list of files returned can then be used as sources for the compilation. 
This way you do not have to specify all source files manually. 
However, this comes with a drawback and is, therefore, not recommended by the CMake
team.

OFFICIAL WARNING:

```
We do not recommend using GLOB to collect a list of source files from your source tree. If no 
CMakeLists.txt file changes when a source is added or removed then the generated build system 
cannot know when to ask CMake to regenerate. 
```

To illustrate the problem try adding a `sub.hpp` and `sub.cpp` to this example and call the function
from main. Make sure to not touch the `CMakeLists.txt` file. During compilation you will get:
`undefined reference to sub(int, int)`. This is due to the fact that the collection is only run at
configure time. So the build system does not know about the newly added files. To resolve the problem
manually trigger a configure in your build folder.

If you want to use `GLOB` you have to be aware of this issue.